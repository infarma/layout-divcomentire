package layout_divcomentire

import (
	"bitbucket.org/infarma/layout-divcomentire/arquivoDePedido"
	divcomentire3 "bitbucket.org/infarma/layout-divcomentire/divcomentire3/arquivoDePedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}

func GetArquivoDePedido3(fileHandle *os.File) (divcomentire3.ArquivoDePedido, error) {
	return divcomentire3.GetStruct(fileHandle)
}
