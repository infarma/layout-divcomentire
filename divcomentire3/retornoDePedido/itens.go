package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	TipoRegistro          int32 `json:"TipoRegistro"`
	EanProduto            int64 `json:"EanProduto"`
	QuantidadeAtendida    int32 `json:"QuantidadeAtendida"`
	QuantidadeNaoAtendida int32 `json:"QuantidadeNaoAtendida"`
	Motivo                int32 `json:"Motivo"`
	Retorno               int32 `json:"Retorno"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.EanProduto, "EanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeAtendida, "QuantidadeAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeNaoAtendida, "QuantidadeNaoAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Motivo, "Motivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Retorno, "Retorno")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":          {0, 1, 0},
	"EanProduto":            {1, 14, 0},
	"QuantidadeAtendida":    {14, 22, 0},
	"QuantidadeNaoAtendida": {22, 30, 0},
	"Motivo":                {30, 32, 0},
	"Retorno":               {32, 33, 0},
}
